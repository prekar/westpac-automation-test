package kiwiSaverCalc.utils;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import io.github.bonigarcia.wdm.WebDriverManager;
import kiwiSaverCalc.pages.BasePage;
import kiwiSaverCalc.pages.CalculatorInfoIcon;
import kiwiSaverCalc.pages.RetirementCalculatorPage;

public class BaseTest {
	
	public static WebDriver driver;
	public static CalculatorInfoIcon calculatorInfoIcon;
	public static RetirementCalculatorPage retirementCalculatorPage;
	public static BasePage basePage;
	
	public static WebDriver getDriver() {
		try {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.get("https://www.westpac.co.nz/");
		//driver.get("https://www.westpac.co.nz/kiwisaver/calculators/kiwisaver-calculator/");
		driver.manage().window().maximize();
		//driver.findElement(By.linkText("Click here to find out how to join")).click();
				//("//*[@help-id='CurrentAge']//button")).click();
		}catch (Exception e) {
			System.out.println("Unable to load browser: " + e.getMessage());
		} finally {
			driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
			calculatorInfoIcon = PageFactory.initElements(driver, CalculatorInfoIcon.class);
			retirementCalculatorPage = PageFactory.initElements(driver, RetirementCalculatorPage.class);
			basePage = PageFactory.initElements(driver, BasePage.class);
		}
		return driver;
	}
		
}

  
