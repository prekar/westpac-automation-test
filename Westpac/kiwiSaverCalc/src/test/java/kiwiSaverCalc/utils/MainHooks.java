package kiwiSaverCalc.utils;

import cucumber.api.java.After;
import cucumber.api.java.Before;

public class MainHooks extends BaseTest {
	
	@Before
	public void setup() {
		driver = getDriver();
	}
	
	@After
	public void tearDown() throws InterruptedException {
		if(driver != null) {
			//driver.manage().deleteAllCookies();
			//driver.close();
			driver.quit();
			driver = null;
		}
	}
	
}
