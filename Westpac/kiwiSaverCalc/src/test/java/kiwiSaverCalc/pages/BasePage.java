package kiwiSaverCalc.pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import kiwiSaverCalc.utils.BaseTest;

public class BasePage extends BaseTest {
	
	public BasePage() throws IOException {
		super();
	}
	
	public static BasePage gotoWestpacWebsite() throws IOException {
		//getDriver().get("https://www.westpac.co.nz/");
		return new BasePage();
	}
	
	public static void navigatetoCalculator() throws InterruptedException {
		Actions actions = new Actions(driver);
		WebElement mainMenu = driver.findElement(By.linkText("KiwiSaver"));
		actions.moveToElement(mainMenu).perform();
		
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='ubermenu-item-cta-kiwisaver-calculators-ps']")));
		WebElement Calcbtn = driver.findElement(By.id("ubermenu-item-cta-kiwisaver-calculators-ps"));
		Calcbtn.click();
		//actions.moveToElement(Calcbtn);
		//actions.click().build().perform();
	}

	public static BasePage retirementcalculator() throws IOException {
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("KiwiSaver Retirement Calculator")));
	driver.findElement(By.linkText("KiwiSaver Retirement Calculator")).click();
	return new BasePage();
	
	}
	
}
