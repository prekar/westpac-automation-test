package kiwiSaverCalc.pages;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ById;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import kiwiSaverCalc.utils.BaseTest;


public class CalculatorInfoIcon extends BaseTest {

	public CalculatorInfoIcon() throws IOException {
		super();
	}

	public static void infoIcon() throws InterruptedException {
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div[1]/div/div[2]/div/div[2]/div/iframe")));
		driver.findElement(By.xpath("/html/body/div/div/div/div[1]/div/div[1]/div/div[1]/div/div/div/div[2]/div[2]/div/div/div/button")).click();
	}

	public static void infoMsg() throws InterruptedException {
		WebElement infomsg = driver.findElement(By.xpath("//*[@class='field-message message-info ng-binding']/p"));
		System.out.print(infomsg);
	}

}
