package kiwiSaverCalc.pages;

import java.io.IOException;

import org.openqa.selenium.By;

import kiwiSaverCalc.utils.BaseTest;

public class RetirementCalculatorPage extends BaseTest {

	public RetirementCalculatorPage() throws IOException{
		super();
	}
	
	public static void enterage(String arg1) {
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div[1]/div/div[2]/div/div[2]/div/iframe")));
		driver.findElement(By.xpath("//*[@id=\"widget\"]/div/div[1]/div/div[1]/div/div[1]/div/div/div/div[2]/div[1]/div[1]/div/div[1]/div/div[1]/div/div/input")).sendKeys(arg1);
		
	}
	
	public static void enteremploymentstatus(String arg1, String arg2) {
		driver.findElement(By.xpath("//*[@ng-model='ctrl.data.EmploymentStatus']//*[@class='control-well'][@ng-click]")).click();
		driver.findElement(By.linkText("Employed")).click(); 
		driver.findElement(By.xpath("//*[@model='ctrl.data.AnnualIncome']")).sendKeys(arg1);
		if (arg2 == "4") {
		driver.findElement(By.id("radio-option-04F")).click();
		}
	}
	public static void selectDefensive() {
		driver.findElement(By.id("radio-option-013")).click();
	}
	
	public static void selectConservative() {
		driver.findElement(By.id("radio-option-016")).click();
	}
	
	public static void selectBalanced() {
		driver.findElement(By.id("radio-option-019")).click();
	}
	
	public static void contribution(String arg1, String arg2, String arg3) {
		driver.findElement(By.xpath("//*[@id=\"widget\"]/div/div[1]/div/div[1]/div/div[5]/div/div/div/div[2]/div[1]/div[1]/div/div/div[1]/div/div/input")).sendKeys(arg1);
		driver.findElement(By.xpath("//*[@id=\"widget\"]/div/div[1]/div/div[1]/div/div[6]/div/div/div/div[2]/div[1]/div[1]/div/div/div[1]/div[1]/div/input")).sendKeys(arg2);
		driver.findElement(By.xpath("//*[@id=\"widget\"]/div/div[1]/div/div[1]/div/div[8]/div/div/div/div[2]/div[1]/div[1]/div/div/div[1]/div/div/input")).sendKeys(arg3);
	}
	
	public static void selfemployed() {
		driver.findElement(By.xpath("//*[@ng-model='ctrl.data.EmploymentStatus']//*[@class='control-well'][@ng-click]")).click();
		driver.findElement(By.linkText("Self-employed")).click(); 
	}
	
	public static void notemployed() {
		driver.findElement(By.xpath("//*[@ng-model='ctrl.data.EmploymentStatus']//*[@class='control-well'][@ng-click]")).click();
		driver.findElement(By.linkText("Not employed")).click(); 
	}
	
	public static void selectfreqfortnightly() {
		driver.findElement(By.xpath("//*[@ng-model='$parent.period']//div[@class='control-well']")).click();
		driver.findElement(By.linkText("Fortnightly")).click();
	}
	
	public static void selectfreqannually() {
		driver.findElement(By.xpath("//*[@ng-model='$parent.period']//div[@class='control-well']")).click();
		driver.findElement(By.linkText("Annually")).click();
	}
	
	public static void calculate() {
		driver.findElement(By.xpath("//*[@id=\"widget\"]/div/div[1]/div/div[2]/button")).click();
	}
}
