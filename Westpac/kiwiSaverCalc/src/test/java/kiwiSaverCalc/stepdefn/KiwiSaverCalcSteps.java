package kiwiSaverCalc.stepdefn;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import kiwiSaverCalc.pages.BasePage;
import kiwiSaverCalc.pages.CalculatorInfoIcon;
import kiwiSaverCalc.pages.RetirementCalculatorPage;
import kiwiSaverCalc.utils.BaseTest;

public class KiwiSaverCalcSteps extends BaseTest{
	
	@Given("^the user opens westpac bank website$")
	public void the_user_opens_westpac_bank_website() throws Throwable {
	    BasePage.gotoWestpacWebsite();
	}

	@Given("^Navigate to Kiwi Calculators$")
	public void navigate_to_Kiwi_Calculators() throws Throwable {
	    BasePage.navigatetoCalculator();
	}

	@Given("^the user go to the retirement calculator page$")
	public void the_user_can_see_the_calculator_details_on_the_page() throws Throwable {
	    BasePage.retirementcalculator();
	}

	@When("^the user clicks on the information icon besides current age field$")
	public void the_user_clicks_on_the_information_icon_besides_current_age_field() throws Throwable {
		CalculatorInfoIcon.infoIcon();
	}

	@Then("^the user can see the valid information message$")
	public void the_user_can_see_the_valid_information_message() throws Throwable {
	    CalculatorInfoIcon.infoMsg();
	}

	@When("^the user whose is of \"([^\"]*)\" , \"([^\"]*)\" at \"([^\"]*)\" pa with KiwiSaver contribution \"([^\"]*)\" %  $")
    public void the_user_whose_is_of_something_something_at_something_pa_with_kiwisaver_contribution_something_(String age, String employmenttype, String salary, String rate) throws Throwable {
		RetirementCalculatorPage.enterage(age);
		if (strArg2 == "Employed") {
		RetirementCalculatorPage.enteremploymentstatus(salary, rate);
		}
		else if (strArg2 == "Self-employed") {
			RetirementCalculatorPage.selfemployed();
		}
		else if (strArg2 == "Not Employed") {
		RetirementCalculatorPage.notemployed();
		}
		else
		System.out.print("Not a valid option for the test");
    }
	
	@When("^has \"([^\"]*)\" with \"([^\"]*)\" \"([^\"]*)\" and chooses \"([^\"]*)\" profile with \"([^\"]*)\"$")
    public void has_something_with_something_something_and_chooses_something_profile_with_something(String currentkiwisaverbal, String voluntarycontribution, String period, String risk, String savingsgoal) throws Throwable {
        if((risk == "Defensive") && (period == "nil")){
        	RetirementCalculatorPage.selectDefensive();
        }
        else if((risk == "Conservative")&& (period == "fortnightly")) {
        	RetirementCalculatorPage.selectConservative();
    		RetirementCalculatorPage.selectfreqfortnightly();
    		RetirementCalculatorPage.contribution(strArg1, strArg2, strArg5);
        }
        else if((risk == "Balanced")&& (period == "annually")) {
        	RetirementCalculatorPage.selectBalanced();
    		RetirementCalculatorPage.selectfreqannually();
    		RetirementCalculatorPage.contribution(strArg1, strArg2, strArg5);
        }
        else
        	System.out.print("Not a valid option for the test");
    }


	@Then("^the user projected balance is calculated at retirement$")
	public void the_user_projected_balance_is_calculated_at_retirement() throws Throwable {
		RetirementCalculatorPage.calculate();
	}


	

}
