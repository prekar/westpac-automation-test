# Westpac Automation Test
How to Execute Test:
1. In the Package Explorer, select the test feature file package
2. Select Run & then Run Configuration
3. On the Main tab, select the appropriate application for that test.
4. Click Run.
Pre-Requisite :
Application must be accessible by chrome browser
Required Tools:
 Java Jdk(7 or higher)
 Eclipse
 Maven
 TestNg

The Framework contains following package
1. kiwiSaverCalc.utils -> This contains runner class web driver configuration and hooks which uses TestNg Annotations
Main Runner classes -> A JUnit Runner is class that extends JUnit's abstract Runner class. Runners are used for running test classes
Main Hooks -> hooks, which are blocks of code that run before or after each scenario USING TestNg
BaseTest -> Initializing Webdrive and Page classes
2. kiwiSaverCalc.stepdefn -> Contains Step defn
KiwiSaverCalcSteps
3. kiwiSaverCalc.featurefile -> Contains Feature file
KiwiSaverCalculator.feature
4. kiwiSaverCalc.pages -> Contains Page based classes
BasePage
CalculatorInfoIcon
RetirementCalculator

Westpac Automation Test